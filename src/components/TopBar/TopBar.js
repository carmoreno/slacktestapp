import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CachedIcon from '@material-ui/icons/Cached';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginBottom: '20px'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'center'
    },
}));


export default function TopBar(props) {
    const classes = useStyles();

    const _handleClick = () => {
        props.onRefresh();
    } 
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Recueperando Mensajes desde Slack
                     </Typography>
                    <IconButton onClick={_handleClick} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <CachedIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    );
}