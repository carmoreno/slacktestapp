import React, { useState, useEffect } from 'react';
import { List, ListSubheader, ListItem, ListItemText, makeStyles } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import GroupIcon from '@material-ui/icons/Group';
import EmailIcon from '@material-ui/icons/Email';
import { User } from '../../Types/User.type';
import { ListItemLight } from '../Lists/ListItemLight';
import { ConversationApi} from '../../API/Conversation';
import { UserApi} from '../../API/User';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 400,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

/**
 * Hook personalizasdo para manejo de peticiones de Miembros del Canal
 */
const useMembers = () => {
    const [membersChannel, setMembersChannel] = useState( [] );
    const userApi = new UserApi();
    const conversationApi = new ConversationApi();

    useEffect(  () => {
        async function getData() {
            const { members } = await conversationApi.getMembers();
            let arrayMembers = [];
                for (let uid of members) {
                    let { user } = await userApi.getProfile(uid);
                    arrayMembers.push(new User(user.real_name, user.name, user.profile.image_original));
                }
            setMembersChannel(arrayMembers);
        }
        getData();
    }, [])

    return membersChannel;
}

export function Aside() {
    const classes = useStyles();
    const members = useMembers();

    return (
        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                    Información Adicional
                </ListSubheader>
            }
            className={classes.root}
        >
            <ListItem button>
                <ListItemIcon>
                    <EmailIcon />
                </ListItemIcon>
                <ListItemText primary="Canal: #Colombia" />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <GroupIcon />
                </ListItemIcon>
                <ListItemText primary="Integrantes" />
            </ListItem>
            <ListItemLight list={members}/>
        </List>
    );
}