import React from 'react';
import { ItemLight } from './ItemLight';
import { List, Grid, makeStyles } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { Message } from '../../Types/Message.type';
import { Attachment } from '../../Types/Attachment.type';
import { User } from '../../Types/User.type';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 800,
        backgroundColor: theme.palette.background.paper,
    },

}));

let _renderList = (list) => {
    const classes = useStyles();
    return (
        <List className={classes.root}>
            {list.map((message, index) => {
                return (
                    <ItemLight key={index} data={message}></ItemLight>
                );
            })}
        </List>
    );
}

export function ListItemLight(props) {

    const { list } = props;
    return (
        <Grid container justify='center'>
            {list.length === 0 ? 'Cargando ...' : _renderList(list)}
        </Grid>
    );
}

ListItemLight.propTypes = {
    list: PropTypes.arrayOf(
        PropTypes.oneOfType([
            PropTypes.instanceOf(Message),
            PropTypes.instanceOf(Attachment),
            PropTypes.instanceOf(User)
        ])
    ).isRequired
}