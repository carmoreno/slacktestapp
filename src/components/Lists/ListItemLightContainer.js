import React from 'react';
import { ListItemLight } from './ListItemLight';

export function ListItemLightContainer(props){

    const { list } = props;
    return <ListItemLight list={list}></ListItemLight>;
    
}