import React from 'react';
import { Grid, ListItem, ListItemAvatar, Avatar, ListItemText, Typography, makeStyles } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { Message } from '../../Types/Message.type';
import { Attachment } from '../../Types/Attachment.type';
import { CardImage } from '../Cards/CardImage';
import { User } from '../../Types/User.type';
 

const useStyles = makeStyles(theme => ({
    inline: {
        display: 'inline',
    },
    grid: {
        margin: 8
    }
}));

function _renderAttachments(attachments) {
    const classes = useStyles();
    return (
        <Grid className={classes.grid} container spacing={2}>
            {attachments? attachments.map( (attach, index) => {
                return (
                    <Grid item key={index}>
                        <CardImage card={attach} />
                    </Grid>
                )
            }) : null}
        </Grid>
    );
}

export function ItemLight(props) {

    const { text, date, user, attachments } = props.data;
    const { name, username, avatar } = user || props.data;

    const classes = useStyles();
    return (
        <div>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp" src={avatar} />
                </ListItemAvatar>
                <ListItemText
                    primary={text}
                    secondary={
                        <React.Fragment>
                            <Typography
                                component="span"
                                variant="body2"
                                className={classes.inline}
                                color="textPrimary"
                            >
                                {`${name.toUpperCase()}`}
                                <Typography component="em"
                                    variant="body2"
                                    className={classes.inline}
                                    color="textPrimary">{`- @${username}`}</Typography>
                            </Typography>
                            {date? ` — ${date}`: null}
                        </React.Fragment>
                    }
                />
            </ListItem>
            {_renderAttachments(attachments)}
        </div>

    );
}

ItemLight.propTypes = {
    data: PropTypes.oneOfType([
        PropTypes.instanceOf(Message),
        PropTypes.instanceOf(Attachment),
        PropTypes.instanceOf(User)
    ]).isRequired
}

