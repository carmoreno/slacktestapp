import React from 'react';
import { Paper, Typography, makeStyles } from '@material-ui/core';
import { indigo } from '@material-ui/core/colors';
const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
        textAlign: 'center',
        marginTop: '10px',
        backgroundColor: indigo[500],
        color: '#ffffff'
    },
}));

export function Footer(props) {
    const classes = useStyles();
    return (
        <Paper className={classes.root}>
            <Typography variant="h5" component="h4">
                Footer
            </Typography>
            <Typography component="p">
                Copyright 2019
            </Typography>
        </Paper>
    );
}