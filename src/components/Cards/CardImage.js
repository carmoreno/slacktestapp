import React from 'react';
import { Card, 
    Typography, 
    CardActionArea, 
    CardMedia, 
    CardContent, 
    CardActions, 
    Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 140,
    },
    media: {
        height: 140,
    },
}));

export function CardImage(props) {
    const classes = useStyles();
    const { name, urlDownload, thumbnail } = props.card;
    return (
        <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={thumbnail}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {name}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size='small' color='primary' href={urlDownload}>Descargar</Button>
            </CardActions>
        </Card>
    );
}