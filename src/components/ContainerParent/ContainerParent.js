import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { Footer } from '../Footer/Footer';
import TopBar from '../TopBar/TopBar';
import { Aside } from '../Aside/Aside';
import { ListItemLightContainer } from '../Lists/ListItemLightContainer';
import { User } from '../../Types/User.type';
import { Message } from '../../Types/Message.type';
import { UserApi } from '../../API/User';
import { ConversationApi } from '../../API/Conversation';

export function ContainerParent(props) {

    const [messageChat, setMessageChat] = useState( [] );
    const conversationApi = new ConversationApi();
    const userApi = new UserApi();
    
    /**
     * Instancia los objetos Message.
     * @param {*} message 
     * @param {*} objUser 
     */
    const getMessage = (message, objUser) => {
        if(message.hasOwnProperty('files')) {
            return new Message(message.text, message.ts, objUser, message.files);
        }
        return new Message(message.text, message.ts, objUser);
    }

    /**
     * Retorna una promesa que resuelve un array de mensajes.
     */
    const getArrayMessage = async() => {
        let { messages } = await conversationApi.getConversations(); // Arreglo de mensajes desde Slack API
        let arrayMessages = [];
        try {         
            for (let message of messages) {
                let { user } = await userApi.getProfile(message.user);
                let objUser = new User (user.real_name, user.name, user.profile.image_original);
                let objMessage = getMessage(message, objUser);
                arrayMessages.push(objMessage);
            }
            return arrayMessages;
        }
        catch (error) {
            console.log('error :', error);
        }
    }

    useEffect( () => {
        async function getData() {
            setMessageChat(await getArrayMessage());
        }
        getData();
    }, [])

    const _onRefresh = async() => {
        setMessageChat(await getArrayMessage());
    }

    return (
        <div>
            <TopBar onRefresh={_onRefresh} />
            <Grid container spacing={1}>
                <Grid item xs={12} md={4}>
                    <Grid container justify='center'>
                        <Aside />
                    </Grid>
                </Grid>
                <Grid item xs={12} md={8}>
                    <ListItemLightContainer list={messageChat} />
                </Grid>
            </Grid>
            <Footer />
        </div>
    );
} 