import { Attachment } from "./Attachment.type";

export class Message {
    constructor(text, date, user, files=[]) {
        this.text = text;
        this.date = new Date(date * 1000).toLocaleString();
        this.user = user;
        this.attachments = this._getAttachments(files);
    }

    _getAttachments(files) {
        let attachments = [];
        for (let file of files) {
            let objAttachments = new Attachment(file.name, file.url_private_download, file.thumb_360)
            attachments.push(objAttachments);
        }
        return attachments;
    }
}