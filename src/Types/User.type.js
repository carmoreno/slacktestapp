export class User {
    constructor(name, username, avatar = '') {
        this.name = name;
        this.username = username;
        this.avatar = avatar;
    }
}