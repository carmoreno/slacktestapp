export class Attachment{
    constructor(name, urlDownload, thumbnail) {    
        this.name = name;
        this.urlDownload = urlDownload;
        this.thumbnail = thumbnail || 'https://via.placeholder.com/150';
    }
}