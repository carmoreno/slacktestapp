import React from 'react';
import './App.css';

import { makeStyles } from '@material-ui/core/styles';


import { ContainerParent } from './components/ContainerParent/ContainerParent';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));


function App() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <ContainerParent></ContainerParent>
        </div>
    );
}

export default App;
