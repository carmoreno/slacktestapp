import { ConfigAPI } from "./Config";

/**
 * Esta clase define los metodos usados para la API de Conversation de Slack
 */
export class ConversationApi extends ConfigAPI {

    /**
     * Peticion a la API de Slack para obtener el historial de conversaciones
     */
    getConversations() {
        return fetch(`https://slack.com/api/conversations.history?token=${this.API_TOKEN}&channel=${this.CHANNEL}&limit=10`)
            .then(res => res.json())
            .catch(error => { console.log('error :', error); });
    }

    /**
     * Peticion para obtener los miembros de un canal
     */
    getMembers() {
        return fetch(`https://slack.com/api/conversations.members?token=${this.API_TOKEN}&channel=${this.CHANNEL}`)
            .then(res => res.json())
            .catch(error => console.log('error :', error));
    }

}