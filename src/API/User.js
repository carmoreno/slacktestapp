import { ConfigAPI } from "./Config";

/**
 * Esta clase define los metodos usados para la API de Users de Slack
 */
export class UserApi extends ConfigAPI {

    /**
     * Retorna una promesa que resuelve 
     *
     * @param {*} uid 
     */
    getProfile(uid) {
        return fetch(`https://slack.com/api/users.info?token=${this.API_TOKEN}&user=${uid}`)
            .then( res => res.json())
            .catch( error => console.log('error :', error));
    }

}